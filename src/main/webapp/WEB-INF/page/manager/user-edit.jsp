<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>编辑用户</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${basePath}/lib/layui-v2.6.3/css/layui.css" media="all">
    <link rel="stylesheet" href="${basePath}/css/public.css" media="all">
    <style>
        body {
            background-color: #ffffff;
        }
    </style>
</head>
<body>
<div class="layui-form layuimini-form">
    <form class="layui-form" action="">
    <div class="layui-form-item">
        <label class="layui-form-label required">用户名称</label>
        <div class="layui-input-block">
            <input type="text" name="username" lay-verify="required" lay-reqtext="用户名称不能为空" placeholder="请输入用户名称" value="${user.username}" class="layui-input">
            <input type="hidden" name="id" value="${user.id}">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label ">真实姓名</label>
        <div class="layui-input-block">
            <input type="text" name="realname"  placeholder="请输入真实姓名" value="${user.realname}" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label required">角色类型</label>
        <div class="layui-input-block">
            <select name="roleId" lay-filter="role" lay-verify="required" lay-reqtext="角色不能为空">
                <option value=""></option>
                <c:forEach items="${roleList}" var="role">
                	<option value="${role.id}" <c:if test="${role.id eq user.roleId }">selected</c:if>>${role.roleName}</option>
                </c:forEach>
            </select>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label ">手机号码</label>
        <div class="layui-input-block">
            <input type="text" name="tel" placeholder="请输入手机号码" value="${user.tel}" class="layui-input" >
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn layui-btn-normal" lay-submit lay-filter="saveBtn">确认修改</button>
        </div>
    </div>
    </form>
    </div>
</div>
<script src="${basePath}/lib/layui-v2.6.3/layui.js" charset="utf-8"></script>
<script>
    layui.use(['form'], function () {
        var form = layui.form,
            layer = layui.layer,
            $ = layui.jquery;

        /*给行政区下拉框绑定change事件*/
        form.on('select(region)',function(data){
            //清空镇（街道）和隔离点的下拉选项
            $("#town").html("<option value=''></option>");
            $("#isolation").html("<option value=''></option>");

            $.ajax({
                type:"get",
                url:"${basePath}/manager/user/list/townAndIsolation.json",
                data:{'regionId':data.value},
                dataType:"json",
                success:function(data){
                    if(data!=null){
                        //初始化镇（街道下拉菜单）
                        if(data.towns!=null && data.towns.length>0){
                            var towns=data.towns;
                            $.each(towns,function (index,item) {
                                $("#town").append(new Option(item.townName,item.id))
                            })
                        }
                        //初始化隔离点下拉菜单
                        if(data.isolations!=null && data.isolations.length>0){
                            var isolations=data.isolations;
                            $.each(isolations,function (index,item) {
                                $("#isolation").append(new Option(item.isolationName,item.id))
                            })
                        }
                    }
                    //重新初始化select组件
                    form.render('select');
                }
            });
        });


        //监听提交
        form.on('submit(saveBtn)', function (data) {
            debugger;
            $.ajax({
            	type:"post",
            	url:"${basePath}/manager/user/update",
            	data:data.field,
            	dataType:"text",
            	success:function(data){
            		if(data=="ok"){
            			layer.alert("修改成功!",function(){
            				//刷新父页面
            				parent.window.location.reload();
            			});
            		}else{
            			layer.alert("修改失败!");
            		}
            	}	
            });
        	//很重要
            return false;
        });

    });
</script>
</body>
</html>