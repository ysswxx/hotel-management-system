package com.hs.mapper;

import com.hs.model.TbSysMenu;

import java.util.List;
import java.util.Map;

public interface TbSysMenuMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TbSysMenu record);

    int insertSelective(TbSysMenu record);

    TbSysMenu selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TbSysMenu record);

    int updateByPrimaryKey(TbSysMenu record);

    List<Map> selectByRoleId(int roleId);
}