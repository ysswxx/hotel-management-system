package com.hs.mapper;

import com.hs.model.TbRolePermission;

public interface TbRolePermissionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TbRolePermission record);

    int insertSelective(TbRolePermission record);

    TbRolePermission selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TbRolePermission record);

    int updateByPrimaryKey(TbRolePermission record);
}