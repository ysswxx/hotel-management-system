package com.hs.mapper;

import com.hs.model.SysPermissionRole;

import java.util.Set;

public interface SysPermissionRoleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysPermissionRole record);

    int insertSelective(SysPermissionRole record);

    SysPermissionRole selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysPermissionRole record);

    int updateByPrimaryKey(SysPermissionRole record);

    Set<String> selectPermissionsByAccount(String account);
}