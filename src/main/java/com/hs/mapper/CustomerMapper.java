package com.hs.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.hs.model.Customer;

public interface CustomerMapper {
    int deleteByPrimaryKey(Integer id);
    
    int deleteByPrimaryKeys(String ids);
    
    int deleteByArray(@Param("ids") Integer[] ids);

    int insert(Customer record);

    int insertSelective(Customer record);

    Customer selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Customer record);

    int updateByPrimaryKey(Customer record);
    
    List<Map<String,Object>> selectByExample(Customer customer);
    
    
}