package com.hs.web.manager;

import com.github.pagehelper.PageInfo;
import com.hs.model.*;
import org.springframework.web.bind.annotation.PathVariable;
import com.hs.service.RoleService;
import com.hs.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/manager")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    /**
     * 跳转到客户列表界面
     * @return
     */
    @RequestMapping("/user/list")
    public String toUserList(Model model){
        //查询行政区 下拉列表
        List<TbSysRegion> regionList=userService.getRegionAll();
        //参数返回页面
        model.addAttribute("regionList",regionList);

        return "manager/user-list";
    }

    /**
     * 获取列表数据
     * @param username
     * @param regionId
     * @param townId
     * @param isolationId
     * @param page  当前要查询的页面
     * @param limit 分页，每一页显示的数量
     * @returnuser/delete
     */
    @RequestMapping("/user/list/data.json")
    @ResponseBody
    public Map getUserListData(String username, Integer regionId, Integer townId, Integer isolationId ,Integer page,Integer limit){
        Map map=new HashMap();
        //查询列表数据
        List<Map> list=userService.getUserListByKeys(username,regionId,townId,isolationId,page,limit);
        PageInfo pageInfo=new PageInfo(list);
        //封装返回接口
        map.put("code","0");
        map.put("msg","");
        map.put("count",pageInfo.getTotal());
        map.put("data",pageInfo.getList());
        return map;
    }

    /**
     * 根据行政区查询街道和隔离点列表
     * @param regionId
     * @return
     */
    @RequestMapping("/user/list/townAndIsolation.json")
    @ResponseBody
    public Map getTownAndIsolationJson(Integer regionId){
        List<TbSysTown> towns=userService.getTownByRegionId(regionId);
        List<TbSysIsolation> isolations=userService.getIsolationByRegionId(regionId);
        Map<String,Object> map=new HashMap<String, Object>();
        map.put("towns",towns);
        map.put("isolations",isolations);
        return map;
    }
    /**
     * 根据行政区名称查询街道列表
     * @param regionName
     * @return
     */
    @RequestMapping("/user/list/town.json")
    @ResponseBody
    public Map getTownJson(String regionName){
        List<TbSysTown> towns=userService.getTownByRegionName(regionName);
        Map<String,Object> map=new HashMap<String, Object>();
        map.put("towns",towns);
        return map;
    }
    /**
     * 跳转到用户添加页面
     * @return
     */
    @RequestMapping("/user/add")
    public String toUserAdd(Model model){
        //查询行政区、角色 下拉列表
        List<TbSysRegion> regionList=userService.getRegionAll();
        List<TbSysRole> roleList=roleService.getRoleAll();
        //参数返回页面
        model.addAttribute("regionList",regionList);
        model.addAttribute("roleList",roleList);
        return "manager/user-add";
    }

    /**
     * 保存用户
     * @param user
     * @return
     */
    @RequestMapping("/user/save")
    @ResponseBody
    public String saveUser(TbSysUser user){
        //判断用户名是否已存在
        TbSysUser hasUser=userService.getUserByUsername(user.getUsername());
        if(hasUser!=null){
            return "exist";
        }
        int result=userService.saveUser(user);
        if(result==1){
            return "ok";
        }
        return "error";
    }

    /**
     * 跳转到用户编辑页面
     * @param model
     * @param username
     * @return
     */
    @RequestMapping("/user/edit/{username}")
    public String editUser(Model model,@PathVariable("username")String username){
        //查询行政区、角色 下拉列表
        /*List<TbSysRegion> regionList=userService.getRegionAll();*/
        List<TbSysRole> roleList=roleService.getRoleAll();
        //参数返回页面
       /* model.addAttribute("regionList",regionList);*/
        model.addAttribute("roleList",roleList);
        TbSysUser user=userService.getUserByUsername(username);
        //获取地区iD，方便编辑页面获取镇（街道）、隔离点
        /*Integer regionId=user.getRegionId();*/
       /* List<TbSysTown> townList=userService.getTownByRegionId(regionId);*/
        /*List<TbSysIsolation> isolationList=userService.getIsolationByRegionId(regionId);*/
        /*model.addAttribute("townList",townList);*/
        /*model.addAttribute("isolationList",isolationList);*/
        model.addAttribute("user",user);
        return "manager/user-edit";
    }

    /**
     * 更新用户信息
     * @param user
     * @return
     */
    @RequestMapping("/user/update")
    @ResponseBody
    public String updateUser(TbSysUser user){
        int result=userService.updateUser(user);
        if(result==1) {
            return "ok";
        }else {
            return "error";
        }
    }

    /**
     * 删除客户
     * @param id
     * @return
     */
    @RequestMapping("/user/delete")
    @ResponseBody
    public String deleteUser(Integer id) {
        int result=userService.deleteUser(id);
        if(result==1) {
            return "ok";
        }else {
            return "error";
        }
    }

    /**
     * 批量删除用户
     * @param ids
     * @return
     */
    @RequestMapping("/user/deleteArray")
    @ResponseBody
    public String deleteArrayUser(Integer[] ids) {
        int result=userService.deleteUsers(ids);
        if(result>0) {
            return "ok";
        }else {
            return "error";
        }
    }

}
