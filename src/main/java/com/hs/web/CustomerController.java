package com.hs.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hs.model.BaseDict;
import com.hs.model.Customer;
import com.hs.service.BaseDictService;
import com.hs.service.CustomerService;
import com.hs.utils.Constants;

@Controller
@RequestMapping("/customer")
public class CustomerController {
	
	//自动注入，以前是要new一个对象的，现在不用了
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private BaseDictService baseDictService;
	/**
	 * 跳转到客户列表页面
	 * @return
	 */
	@RequestMapping("/list")
	public String customerList(Model model) {
		//查询客户来源，所属行业，客户级别字典列表
		List<BaseDict> sourceDict=baseDictService.getBaseDictListByCode(Constants.CON_cust_source);
		List<BaseDict> industryDict=baseDictService.getBaseDictListByCode(Constants.CON_cust_industry);
		List<BaseDict> levelDict=baseDictService.getBaseDictListByCode(Constants.CON_cust_level);
		model.addAttribute("sourceDict",sourceDict);
		model.addAttribute("industryDict",industryDict);
		model.addAttribute("levelDict",levelDict);
		return "customer-list";
	}
	/**
	 * 跳转到客户添加页面
	 * @return
	 */
	@RequestMapping("/add")
	public String customerAdd(Model model) {
		//查询客户来源，所属行业，客户级别字典列表
		List<BaseDict> sourceDict=baseDictService.getBaseDictListByCode(Constants.CON_cust_source);
		List<BaseDict> industryDict=baseDictService.getBaseDictListByCode(Constants.CON_cust_industry);
		List<BaseDict> levelDict=baseDictService.getBaseDictListByCode(Constants.CON_cust_level);
		model.addAttribute("sourceDict",sourceDict);
		model.addAttribute("industryDict",industryDict);
		model.addAttribute("levelDict",levelDict);
		return "customer-add";
	}
	
	/**
	 * 跳转到客户编辑页面
	 * @return
	 */
	@RequestMapping("/edit/{id}")
	public String customerEdit(Model model,@PathVariable("id") Integer id) {
		//查询客户信息
		Customer customer= customerService.getCustomerById(id);
		//查询客户来源，所属行业，客户级别字典列表
		List<BaseDict> sourceDict=baseDictService.getBaseDictListByCode(Constants.CON_cust_source);
		List<BaseDict> industryDict=baseDictService.getBaseDictListByCode(Constants.CON_cust_industry);
		List<BaseDict> levelDict=baseDictService.getBaseDictListByCode(Constants.CON_cust_level);
		model.addAttribute("sourceDict",sourceDict);
		model.addAttribute("industryDict",industryDict);
		model.addAttribute("levelDict",levelDict);
		model.addAttribute("customer",customer);
		return "customer-edit";
	}	
	/**
	 * 保存客户
	 * @param customer
	 * @return
	 */
	@RequestMapping("/save")
	@ResponseBody
	public String customerSave(Customer customer) {
		int result=customerService.saveCustomer(customer);
		if(result==1) {
			return "ok";
		}else {
			return "error";
		}	
	}

	@ResponseBody
	@RequestMapping("/list/data")	
	public Map<String,Object> customerListJson(Customer customer) {
		List<Map<String, Object>> list=customerService.getCustomerListByKeyword(customer);
		Map<String,Object> map=new HashMap();
		map.put("code", 0);//请求成功
		map.put("msg","");
		map.put("count",list.size());
		map.put("data",list);
		return map;
	}
	
	/**
	 * 更新客户信息
	 * @param customer
	 * @return
	 */
	@RequestMapping("/update")
	@ResponseBody
	public String customerUpdate(Customer customer) {
		int result=customerService.updateCustomer(customer);
		if(result==1) {
			return "ok";
		}else {
			return "error";
		}	
	}
	
	/**
	 * 删除客户
	 * @param customer
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public String customerDelete(Integer id) {
		int result=customerService.deleteCustomer(id);
		if(result==1) {
			return "ok";
		}else {
			return "error";
		}	
	}
	
	/**
	 * 批量删除客户信息
	 * @param ids
	 * @return
	 */
	@RequestMapping("/deleteAll")
	@ResponseBody
	public String customersDelete(String ids) {
		int result=customerService.deleteCustomers(ids);
		if(result>0) {
			return "ok";
		}else {
			return "error";
		}	
	}
	
	@RequestMapping("/deleteArray")
	@ResponseBody
	public String customersDeleteArray(Integer[] ids) {
		int result=customerService.deleteCustomers(ids);
		if(result>0) {
			return "ok";
		}else {
			return "error";
		}	
	}
	
	
}
