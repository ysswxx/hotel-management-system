package com.hs.service;

import com.hs.model.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface UserService {
    public TbSysUser getUserByUsername(String username);

    public Set<String> getRoleByAccount(String account);

    public Set<String> getPermissionsByAccount(String account);

    public List<Map> getUserListByKeys(String username,Integer regionId,Integer townId,Integer isolationId,Integer page,Integer limit);

    public List<TbSysRegion> getRegionAll();

    public List<TbSysTown> getTownByRegionId(Integer regionId);

    public List<TbSysTown> getTownByRegionName(String regionName);

    public List<TbSysIsolation> getIsolationByRegionId(Integer regionId);

    public List<TbSysTown> getAllTown();

    public int saveUser(TbSysUser user);

    public int updateUser(TbSysUser user);

    public int deleteUser(Integer id);

    public int deleteUsers(Integer[] ids);
}
