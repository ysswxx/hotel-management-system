package com.hs.service.impl;

import com.github.pagehelper.PageHelper;
import com.hs.mapper.*;
import com.hs.model.TbSysIsolation;
import com.hs.model.TbSysRegion;
import com.hs.model.TbSysTown;
import com.hs.model.TbSysUser;
import com.hs.service.UserService;
import com.hs.utils.MD5Utils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    //注入Mapper接口动态代理对象
    @Autowired
    private TbSysUserMapper tbSysUserMapper;
    @Autowired
    private SysRoleMapper sysRoleMapper;
    @Autowired
    private SysPermissionRoleMapper sysPermissionRoleMapper;
    @Autowired
    private TbSysRegionMapper regionMapper;
    @Autowired
    private TbSysTownMapper townMapper;
    @Autowired
    private TbSysIsolationMapper isolationMapper;
    @Value("#{properties['user.password']}")
    private String defaultPassword;

    /**
     * 根据用户名查询用户信息
     * @param username
     * @return
     */
    @Override
    public TbSysUser getUserByUsername(String username) {
        if(StringUtils.isNotBlank(username)){
            return tbSysUserMapper.selectByUsername(username);
        }
        return null;
    }

    /**
     * 根据用户名查询角色
     * @param account
     * @return
     */
    @Override
    public Set<String> getRoleByAccount(String account) {
        if(StringUtils.isNotBlank(account)){
            return sysRoleMapper.selectRolesByAccount(account);
        }
        return null;
    }

    /**
     * 根据用户名查询权限
     * @param account
     * @return
     */
    @Override
    public Set<String> getPermissionsByAccount(String account) {
        if(StringUtils.isNotBlank(account)){
            return sysPermissionRoleMapper.selectPermissionsByAccount(account);
        }
        return null;
    }

    /**
     * 根据条件查询账户列表
     * @param username
     * @param regionId
     * @param townId
     * @param isolationId
     * @return
     */
    @Override
    public List<Map> getUserListByKeys(String username, Integer regionId, Integer townId, Integer isolationId,Integer page,Integer limit) {
        //执行分页查询
        PageHelper.startPage(page,limit);
        return tbSysUserMapper.selectByKeys(username,regionId,townId,isolationId);
    }

    /**
     * 查询隔离点列表
     * @return
     */
    @Override
    public List<TbSysRegion> getRegionAll() {
        return regionMapper.selectAll();
    }

    /**
     * 查询镇（街道）列表
     * @param regionId
     * @return
     */
    @Override
    public List<TbSysTown> getTownByRegionId(Integer regionId) {
        return townMapper.selectByRegionId(regionId);
    }


    @Override
    public List<TbSysTown> getTownByRegionName(String regionName) {
        return townMapper.selectByRegionName(regionName);
    }

    @Override
    public List<TbSysTown> getAllTown() {
        return townMapper.selectAllTown();
    }

    /**
     * 查询隔离点列表
     * @return
     */
    @Override
    public List<TbSysIsolation> getIsolationByRegionId(Integer regionId) {
        return isolationMapper.selectByRegionId(regionId);
    }

    /**
     * 保存用户
     * @param user
     * @return
     */
    @Override
    public int saveUser(TbSysUser user) {
        //设置默认密码并加密
        user.setPassword(MD5Utils.getMD5(defaultPassword));
        return tbSysUserMapper.insert(user);
    }

    /**
     * 更新用户信息
     * @param user
     * @return
     */
    @Override
    public int updateUser(TbSysUser user) {
        return tbSysUserMapper.updateByPrimaryKeySelective(user);
    }

    /**
     * 删除用户
     * @param id
     * @return
     */
    @Override
    public int deleteUser(Integer id) {
        return tbSysUserMapper.deleteByPrimaryKey(id);
    }

    /**
     * 批量删除用户
     * @param ids
     * @return
     */
    @Override
    public int deleteUsers(Integer[] ids) {
        int result=0;
        if(ids.length>0) {
            result=tbSysUserMapper.deleteByArray(ids);
        }
        return result;
    }
}
